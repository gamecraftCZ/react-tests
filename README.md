## IDE config Instructions

##### 1. Setup prettier

- Visual Studio Code

  - Install "prettier" plugin
  - Go to settings
  - Turn on prettier "format on save"
  - Turn on prettier "require config"

- Web Storm (IntelliJ)
  - Install "prettier" plugin
  - Record macro for prettify and save (Edit -> macros -> Start macro recording)
    - Reformat with prettify (Ctrl + Alt + Shift + P)
    - Save (Ctrl + S)
  - Bind macro to Save (Ctrl + S) instead of original save  
    (File -> Settings -> Keymap -> Macros -> {Your macro} -> Add keyboard shortcut)

##### 2. Setup ESLint

- Visual Studio Code
  - Install "ESLint" plugin
- Web Storm (IntelliJ)
  - Go to Settings
  - Change ESLint to Manual ESLint configuration

##### 3. Install dependencies

- `yarn install`

## Git instructions

- Everyone should have his own branch to work on.
- Prefix every commit with either
  - `B:` if it has changes on Backend
  - `F:` if it has changes on Frontend
  - `B-F:` if it has changes on both Backend and Frontend
