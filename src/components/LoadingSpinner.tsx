import React from "react";
import Loader from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

type LoadingSpinnerProps = { text?: string; color?: string };
const LoadingSpinner = (props: LoadingSpinnerProps) => {
  const color = props.color || "#55be6e";

  return (
    <div className="vertical-flex">
      <h3>{props.text}</h3>
      <Loader type="Oval" color={color} />
    </div>
  );
};

export default LoadingSpinner;
