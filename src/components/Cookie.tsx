import React from "react";
import cookieImage from "../media/cookie.png";

type CookieProps = { name: string; size: string };
const Cookie: React.FC<CookieProps> = (props: CookieProps) => {
    return (
        <div className="list-item">
            <img src={cookieImage} alt="cookie" />
            <h2>{props.name}</h2>
            <h5>size: {props.size}</h5>
        </div>
    );
};

export default Cookie;
