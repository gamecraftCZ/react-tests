import React from "react";
import useDropdown from "../hooks/useDropdown";
import useTextInput from "../hooks/useTextInput";

const cookieSizes = ["Extra small", "Small", "Medium", "Large", "Extra large"];

const SearchField: React.FC = () => {
    const [cookieSize, CookieSizeDropdown] = useDropdown("Cookie Size: ", "", cookieSizes);
    const { Input: SearchInput, value: searchValue } = useTextInput("search");
    const { Input: DataInput, value: dataValue } = useTextInput("data", {
        exactLength: 9,
        number: true,
    });
    const { Input: TestInput, value: testValue } = useTextInput("test");

    return (
        <div>
            <form>
                <SearchInput
                    label={searchValue}
                    prefix="Hledej> "
                    config={{ placeholder: "search" }}
                />
                <br />

                <DataInput
                    label={dataValue}
                    prefix="(9 characters, number)"
                    config={{ placeholder: "data" }}
                />
                <br />

                <TestInput label={testValue} config={{ placeholder: "test" }} />
                <br />

                <CookieSizeDropdown />
                <h5>Selected cookie size: {cookieSize}</h5>
            </form>
        </div>
    );
};

export default SearchField;
