import React, { useMemo, useState } from "react";
import { StringValidationOptions, validateString } from "./stringValidation";
import { createTextInput } from "./TextInput";

type useInputComponentProps = {
    config?: JSX.IntrinsicElements["input"];
    label?: string;
    prefix?: string;
};
const useTextInput = (
    defaultValue: string,
    validation?: StringValidationOptions,
): {
    Input: React.ElementType<useInputComponentProps>;
    value: string;
    isValid: boolean;
    isTouched: boolean;
} => {
    const [value, setValue] = useState(defaultValue);
    const [isTouched, setIsTouched] = useState(false);
    const [isValid, setIsValid] = useState(useMemo(() => validateString(value, validation), []));

    const [Input] = useState(() =>
        createTextInput(defaultValue, validation, setValue, setIsValid, setIsTouched),
    );

    return { Input, value: value, isValid, isTouched };
};

export default useTextInput;
