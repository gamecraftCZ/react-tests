import React, { useMemo, useState } from "react";
import "./TextInput.sass";
import { StringValidationOptions, validateString } from "./stringValidation";

type useInputComponentProps = {
    config?: JSX.IntrinsicElements["input"];
    label?: string;
    prefix?: string;
};
export const createTextInput = (
    defaultValue: string,
    validationOptions: StringValidationOptions,
    setValue: (value: string) => void,
    setIsValid: (value: boolean) => void,
    setIsTouched: (value: boolean) => void,
) =>
    function TextInput(props: useInputComponentProps) {
        const [value, setInternalValue] = useState(defaultValue);
        const [isTouched, setInternalIsTouched] = useState(false);
        const [isValid, setInternalIsValid] = useState(
            useMemo(() => validateString(value, validationOptions), []),
        );

        const setValueAndValidate = (newValue: string) => {
            setInternalValue(newValue);
            setValue(newValue);

            const isValid = validateString(newValue, validationOptions);
            setInternalIsValid(isValid);
            setIsValid(isValid);

            if (!isTouched) {
                setInternalIsTouched(true);
                setIsTouched(true);
            }
        };
        const input = (
            <input
                className={`TextInput ${isTouched && !isValid ? "TextInputInvalid" : ""}`}
                {...props.config}
                value={value}
                onChange={e => {
                    setValueAndValidate(e.target.value);
                }}
            />
        );
        const withLabel = props.label ? (
            <label>
                {props.label} {props.prefix} {input}
            </label>
        ) : (
            <>
                {props.prefix} {input}
            </>
        );
        return <div>{withLabel}</div>;
    };
