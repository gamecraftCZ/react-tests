import { objectEntries } from "../utils/object";
import { isNumericBase10 } from "../utils/basicUtils";

export interface StringValidationOptions {
    required?: boolean;
    minLength?: number;
    maxLength?: number;
    exactLength?: number;
    number?: boolean;
    format?: RegExp;
    customValidation?: (value: string) => boolean;
}

export const stringValidationHandlers: {
    [Key in keyof StringValidationOptions]: (
        str: string,
        value: StringValidationOptions[Key],
    ) => boolean;
} = {
    required: (str, value) => str.length !== 0,
    minLength: (str, value) => str.length >= value,
    maxLength: (str, value) => str.length <= value,
    exactLength: (str, value) => str.length === value,
    number: (str, value) => isNumericBase10(str),
    format: (str, value) => value.test(str),
    customValidation: (str, value) => value(str),
};

export const validateString = (str: string, options: StringValidationOptions) => {
    if (!options) return true;
    return objectEntries(options).every(([key, value]) =>
        stringValidationHandlers[key](
            str,
            // @ts-ignore // This code is valid, but TypeScript's type inference isn't strong enough to determine that and therefore reports error instead
            value,
        ),
    );
};
