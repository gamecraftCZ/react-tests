import React, { useState } from "react";
import { arrayNotContains } from "../utils/basicUtils";

const useDropdown = (
  label: string,
  defaultState: string,
  options: string[]
): [string, Function, Function] => {
  if (defaultState === "" && arrayNotContains(options, defaultState)) {
    defaultState = options[0];
  }

  const [state, setState] = useState(defaultState);
  const id = `use-dropdown-${label.replace(" ", "").toLowerCase()}`;
  const Dropdown = () => (
    <label htmlFor={id}>
      {label}
      <select
        id={id}
        value={state}
        onChange={e => setState(e.target.value)}
        onBlur={e => setState(e.target.value)}
        disabled={options.length === 0}
      >
        {options.map((item: string) => (
          <option key={item}>{item}</option>
        ))}
      </select>
    </label>
  );

  return [state, Dropdown, setState];
};

export default useDropdown;
