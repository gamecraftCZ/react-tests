import React from "react";
import "./App.sass";
import "./Custom.sass";
import Cookie from "./components/Cookie";
import SearchField from "./components/SearchField";
import LoadingSpinner from "./components/LoadingSpinner";

const App: React.FC = () => {
  return (
    <div className="App">
      <SearchField />
      <LoadingSpinner text="Infinity loading..." />
      <div className="flexible-list">
        <Cookie name="Tonda" size="100 cm" />
        <Cookie name="Michal" size="5 mm" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda Je Borec" size="6 dm" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda" size="16 m" />
        <Cookie name="Strejda" size="16 m" />
      </div>
    </div>
  );
};

export default App;
