const arrayContains = <T>(array: T[], item: T): boolean => {
  array.forEach((i: T) => {
    if (i === item) return true;
  });

  return false;
};
const arrayNotContains = <T>(array: T[], item: T): boolean => {
  return !arrayContains(array, item);
};

/**
 * Checks if string is a valid number in Base 10.
 * @param text
 * @returns {boolean}
 */
const isNumericBase10 = (text: string): boolean => {
  for (const char of text) {
    if (!"0123456789".includes(char)) return false;
  }
  return true;
};

export { arrayContains, arrayNotContains, isNumericBase10 };
