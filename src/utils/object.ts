/**
 * Gets entries of given object
 *
 * The result is identical to the result of `Object.entries(obj)`, but is asserted to be different type.
 * This type cast is often necessary, since `Object.entries` has "incorrectly" specified return type in `lib.es2017.object.d.ts` and infers `Object.entries(obj: Record<T, K>): [string, unknown][]`.
 * The clarification why is that done is here: https://github.com/microsoft/TypeScript/pull/12253#issuecomment-263132208
 */
export const objectEntries = <TKey extends keyof any, TValue>(obj: Partial<Record<TKey, TValue>>) =>
    Object.entries(obj) as [TKey, TValue][];
