
const Cookie = (props) => {
    return new React.createElement(
        "div",
        {className: "cookie"},
        new React.createElement(
            "img",
            {src: "./cookie.png"},
        ),
        new React.createElement(
            "h2",
            {},
            props.name,
        ),
        new React.createElement(
            "h5",
            {},
            `size: ${props.size}`,
        ),
    );
};

const Cookies = () => {
    return React.createElement(
        "div",
        {className: "cookies"},
        React.createElement(Cookie, {name: "Tonda", size: "100 cm"}),
        React.createElement(Cookie, {name: "Michal", size: "5 mm"}),
        React.createElement(Cookie, {name: "Strejda", size: "16 m"}),
    )
};

const App = () => {
    return React.createElement(
        "div",
        {},
        React.createElement("h1", {}, "HEY!"),
        React.createElement(Cookies, {}),
    )
};